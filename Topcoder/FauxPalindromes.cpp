#include <string>
#include <vector>
#include <list>
#include <cmath>
#include <algorithm>
#include <map>
#include <set>
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

class FauxPalindromes {
  public:
    string classifyIt(string word);
};

string FauxPalindromes::classifyIt(string word)
{
  string rword, p, r;
  size_t i;
  int j;
  for (j = word.size()-1; j >= 0; j--) rword.push_back(word[j]); //rword is the reverse of word.
  if (word == rword) return "PALINDROME";
  p.push_back(word[0]);
  for (i = 1; i < word.size(); i++) {
    if (word[i] != word[i-1]) p.push_back(word[i]);
  }

  for (j = p.size()-1; j >= 0; j--) r.push_back(p[j]);               // r is the reverse of p.

  if (r == p) return "FAUX";
  return "NOT EVEN FAUX";
}
